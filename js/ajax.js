/**
 * @file
 * Allows views to be loaded via data attributes..
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.lazyViewsAjax = {
    attach: function (context) {
      // Attach ajax action to any item.
      const elements = once('attach-ajax-links', '[data-lv-id]', context);
      elements.forEach(this.attachLazyAjax);
    },
    attachLazyAjax: function (item, idx) {
      var $el = $(item);

      var view_id = $el.attr('data-lv-id');
      var view_display = $el.attr('data-lv-display') || false;

      // Need a display.
      if (!view_id || !view_display) return;

      // @todo move these to a defaults extend.
      var view_args = $el.attr('data-lv-args') || {};
      var view_dom_id = $el.attr('data-lv-target') || 'lazy-view';
      var execute = $el.attr('data-lv-execute') || false;
      var method = $el.attr('data-lv-type') || 'POST';

      // Allows for not adding the prefix
      var target_exists = $('.js-view-dom-id-' + view_dom_id).length;
      if (!target_exists) {
        var $target = $('.' + view_dom_id + ', #' + view_dom_id);
        if ($target.length) {
          target_exists = true;
          $target.addClass('js-view-dom-id-' + view_dom_id);
        }
      }

      // Do nothing if no placholder.
      if (target_exists) {

        // Everything we need to specify about the view.
        var view_info = {
          view_name: view_id,
          view_display_id: view_display,
          view_args: view_args,
          view_dom_id: view_dom_id,
        };

        // Details of the ajax action.
        var ajax_settings = {
          submit: view_info,
          url: drupalSettings.path.baseUrl + 'views/ajax',
          element: item,
          event: execute || 'click',
          progress: { type: 'fullscreen' },
          httpMethod: method,
        };

        var ajax_call = Drupal.ajax(ajax_settings);

        // Load right away.
        if (execute) {
          ajax_call.execute();
        }
      }
    }
  };

})(jQuery, Drupal);

